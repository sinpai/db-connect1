<?php
class DbViewer {
	private $mysqli;
	
	function __construct($mysqli) {
		$this->mysqli = $mysqli;
	}
	
    // Функция для показа последних n записей из таблицы
    function last_rows ($table_name, $number) {
        echo "\n----- {$number} LAST ROWS ------\n";

        $query_result = $this->mysqli->query("SELECT * FROM {$table_name} ORDER BY id DESC LIMIT {$number}");
        echo "|----|----------|------------->>\n";
        echo "| ID | Username | Description >>\n";

        while ( $query_row = $query_result->fetch_assoc() ) {
            $format = "| %s | %s | %s |\n";
            echo sprintf($format, str_pad($query_row['id'], 2), str_pad($query_row['Username'], 8, " ", STR_PAD_BOTH), str_pad($query_row['Description'], 11," ", STR_PAD_BOTH));
        };
        echo "\n----- END OF {$number} LAST ROWS ------\n";
    }

    // Функция для красивого вывода всех значений в таблице
    function show_all ($table_name) {
        echo "\n------ FINAL QUERY -------\n";

        $rows_after_change = $this->mysqli->query("SELECT * FROM test");
        echo "|----|----------|------------->>\n";
        echo "| ID | Username | Description >>\n";

        foreach($rows_after_change as $row) {
            $format = "| %s | %s | %s |\n";
            echo sprintf($format, str_pad($row['id'], 2), str_pad($row['Username'], 8, " ", STR_PAD_BOTH), str_pad($row['Description'], 11," ", STR_PAD_BOTH));
        }
    }
}
?>