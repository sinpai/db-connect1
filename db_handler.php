<?php
class DbHandler {
	private $mysqli;

	function __construct($mysqli) {
		$this->mysqli = $mysqli;
	}

    // Функция для добавления шаблонных значений в базу
    function add_test_values ($table_name, $rows_number, $prefix = 'user', $description ) {
		for ( $i = 0 ; $i < $rows_number; $i++) {
			$performing_query = $this->mysqli->query("INSERT INTO {$table_name}(id, Username, Description) VALUES ({$i}, '{$prefix}{$i}', '{$description}{$i}')") or trigger_error("Не удалось добавить записи:" . $performing_query . $this->mysqli->error);
		}
	}

    // Функция для обновления данных по заданным столбцам через массив с изменениями.
    // $columns содержит имя колонок для обновления
    // $data_array имеет структуру array(<номер записи> => <новые значения для столбцов через запятую>)
    function update_data ($table_name, $columns, $data_array) {
        foreach($data_array as $row_number => $column_changes) {
            $data = explode(', ', $column_changes);
            $this->mysqli->query("UPDATE {$table_name} SET " .
                           $columns[0] . " = '" . $data[0] . "', " .
                           $columns[1] . " = '" . $data[1] .
                           "' WHERE id = " . $row_number);
        }
    }
}
?>