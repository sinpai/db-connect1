<?php
// Класс для манипуляций с базой через родной коннектор mysqli
class DbConnection {
	
	private $mysqli;

	// Подключение базы данных
	function connect($DB_ADDRESS, $DB_USER, $DB_PASS, $DB_NAME) {
  		$this->mysqli = new mysqli($DB_ADDRESS, $DB_USER, $DB_PASS, $DB_NAME);
    	if ($this->mysqli->connect_errno) {
			echo "Не удалось подключиться к MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
		}
		return $this->mysqli;
	}    

	// Функция для закрытия соединения с переданным объектом соединения
	function close_connection(){
		$this->mysqli->close();
	}
}
?>