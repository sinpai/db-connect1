<?php
class DbCreator {
	private $mysqli;
	
	function __construct($mysqli) {
		$this->mysqli = $mysqli;
	}
	
    // Функция для добавления новой таблицы с именем и параметрами
    function create_new_table ($name, $query) {
        $this->mysqli->query("DROP TABLE IF EXISTS test");
        $this->mysqli->query("CREATE TABLE {$name}({$query})");
        if ($this->mysqli->errno) {
            echo "Не удалось создать таблицу: (" . $this->mysqli->errno . ") " . $mysqli->error;
        }
    }
}
?>