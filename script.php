<?php
include 'db_connection.php';
include 'db_creator.php';
include 'db_handler.php';
include 'db_viewer.php';	
include 'variables.php';

$db_oper = new DbConnection;
$db_obj = $db_oper->connect($DB_ADDRESS, $DB_USER, $DB_PASS, $DB_NAME);
$db_creator = new DbCreator($db_obj);
$db_handler = new DbHandler($db_obj);
$db_viewer = new DbViewer($db_obj);

// Create new table
$table_name = 'test';
$db_creator->create_new_table($table_name, 'id INT, Username nvarchar(20), Description nvarchar(100)');

// Add Values
$db_handler->add_test_values($table_name, 5, 'user', 'Quisquam animi odit magni beatae corrupti libero.');

// View last 5 rows
$db_viewer->last_rows($table_name, 10);

// Update rows
$db_handler->update_data($table_name,
                      array('Username', 'Description'),
                      array(0 => 'nordman, text text',
                            1 => 'bookkk, text text 2',
                            8 => 'hoonk, null'));
							
// Show all values
$db_viewer->show_all($table_name);

// Close connection
$db_oper->close_connection();
?>